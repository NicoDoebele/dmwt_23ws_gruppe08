"use client"

import useSWR from "swr";

interface Comment {
    postID: number;
    id: number;
    name: string;
    email: string;
    body: string;
}

function Comment({ comment }: { comment: Comment }) {
    return (
        <li key={comment.id} className="flex flex-col">
            <h2 className="text-xl font-semibold">{comment.name}</h2>
            <p>{comment.body}</p>
            <p className="font-light">{comment.email}</p>
        </li>
    )
}

export default function Comments() {

    const fetcher = (url: string) => fetch(url).then((res) => res.json() as Promise<Comment[]>);
    const { data, error } = useSWR("/api/comments", fetcher);

    const comments = data?.slice(0, 10);

    if (error) return <div>Failed to load</div>;

    if (!data) return <div>Loading...</div>;

    return (
      <div className="w-full flex flex-col items-center gap-16 py-24">
        <h1 className="text-3xl font-semibold">Kommentare</h1>
        <ul className="flex flex-col gap-4">
          {comments?.map((comment) => (
            <Comment comment={comment} />
          ))}
        </ul>
      </div>
    );
  }
  