export async function GET(req: Request) {
    const res = await fetch("https://jsonplaceholder.typicode.com/comments");
    const comments = await res.json();
    return  Response.json(comments);
}