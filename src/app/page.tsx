import Image from 'next/image';

export default function Home() {
  return (
    <main className="bg-white">

      <div id='main-section-one' className="w-full flex">
        <div className="w-1/2 h-full">
          <Image src="/Hero.png" alt="Hero" width={840} height={960} className='w-full aspect-auto' />
        </div>
        <div className="w-1/2 aspect-[840/960] flex justify-center items-center">
          <h1 className='text-[56px] w-[686px]'>
            Ein starker Geist folgt einem starken Körper
          </h1>
        </div>
      </div>

      <div id='main-section-two' className='w-full aspect-[1680/600] flex'>
        <div className='w-1/2 flex flex-col items-center justify-center'>
          <div className='w-[686px] flex flex-col gap-4'>
            <h2 className='text-[24px] font-bold'>
              Lerne, wie du deinen Körper gesund ernährst
            </h2>
            <span className='text-[18px]'>
              Dein Körper ermöglicht dir vieles in dieser Welt. Wenn du willst, dass es weiterhin so bleibt, solltest du dich möglichst gut um ihn kümmern. Mit einer ausgewogenen Ernährung kommst du weiter im Leben.
            </span>
          </div>
        </div>
        <div className='w-1/2 h-full flex items-center justify-start'>
          <Image src="/Content.png" alt="Content" width={750} height={440} className='w-[90%]' />
        </div>
      </div>

    </main>
  );
}
