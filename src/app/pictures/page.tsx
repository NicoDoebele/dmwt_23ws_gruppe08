import Image from 'next/image'

function Picture({ url, alt, title, isGutbrod }: { url: string, alt: string, title: string, isGutbrod?: boolean}) {

    const pictureClass = isGutbrod ? "w-[400px] rounded-2xl shadow-2xl" : "w-[400px] rounded-2xl border-2"

    return (
        <div className="flex flex-col items-center gap-4">
            <Image width={400} height={isGutbrod ? 400 : 532} src={url} alt={alt} className={pictureClass} />
            <h2 className="text-xl font-semibold">{title}</h2>
        </div>
    )

}

export default function Pictures() {
    return (
        <div className="w-full flex flex-col justify-around flex-wrap items-center gap-8 py-24">

            <div className='flex flex-col items-center'>
                <h1 className="text-3xl font-semibold flex justify-center">Gründer von DFT Fitness</h1>
                <span className='text-gray-400'>Disclaimer: Dies sind nicht wirklich die Gründer von DFT Fitness</span>
                <span className='text-gray-300 font-light'>Disclaimer: DFT Fitness existiert (noch) nicht</span>
            </div>

            <div className="w-full flex justify-around flex-wrap items-center gap-16">
                <Picture url="/pictures-page/gutbrod.jpg" alt="Gutbrod" title="Gutbrod" isGutbrod />
                <Picture url="/pictures-page/nico.webp" alt="Nico" title="Nico" />
                <Picture url="/pictures-page/robin.webp" alt="Robin" title="Robin" />
                <Picture url="/pictures-page/marios.webp" alt="Marios" title="Marios" />
            </div>

            <h2 className="text-3xl font-semibold">Unsere lieblings CSS Elemente</h2>

            <div className="w-full flex justify-around flex-wrap items-center gap-16">
                <div id="css-element-einz" className="w-[400px] aspect-square border-2 border-red-600 rounded-2xl" />
                <div id="css-element-zwei" className="w-[400px] aspect-square shadow-2xl rounded-2xl" />
            </div>
        </div>
    );
  }
  