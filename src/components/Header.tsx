import Link from "next/link";
import Image from "next/image";

export default function Header() {
    return (
        <header className="w-full h-[60px] px-[50px] flex items-center">
            <nav className="w-full flex justify-between items-center">
                <div>
                    <Link href="/" className="flex items-center gap-[10px] text-[20px]">
                        <Image src="/logo.png" alt="Logo" className="h-[40px] w-[40px]" width={40} height={40} />
                        <span>Protein Bros</span>
                    </Link>
                </div>

                <div className="flex gap-10 text-[15px]">
                    <Link href="/comments">Kommentare</Link>
                    <Link href="/pictures">Bilder</Link>
                </div>
            </nav>
        </header>
    )
}