import Image from 'next/image';
import Link from 'next/link';

export default function Footer() {
    return (
        <footer className="w-full h-[264px] px-[50px] flex items-center justify-between">
            <div>
                <Link href="/" className="flex items-center gap-[10px] text-[20px]">
                    <Image src="/logo.png" alt="Logo" className="h-[40px] w-[40px]" width={40} height={40} />
                    <span>Protein Bros</span>
                </Link>
            </div>
            
            <div className="flex gap-16 text-[15px] font-bold">
                <Link href="/contact">
                    Kontakt
                </Link>

                <Link href="/imprint">
                    Impressum
                </Link>

                <Link href="/privacy-policy">
                    Datenschutz
                </Link>
            </div>

            <div>
                <span className="text-[15px]">© DFT Fitness</span>
            </div>
        </footer>
    )
}